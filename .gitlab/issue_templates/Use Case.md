**Actor**

*Add primary actor of use case*

Examples:  
Network administrator  
Groundstation owner  


**Description**

*Add description of use case*

Example:
The actor wants to give permissions to users (not station owners) on network.satnogs.org to schedule observations on the actor’s ground station.

**Scenario**

*Add scenario of use case*

Example:
1. A user wants to configure the system.
2. The user changes the configuration.
3. The user restart the system.
4. The system operates with the new configuration.

/label ~"Use Case"
